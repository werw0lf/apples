import reducer from '../../reducers'
import types from '../../constants/actions'
describe('main reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
        users: [],
        basket: [],
        error: '',
        showError: false,
    })
  })

  it('should handle SET_USERS', () => {
    expect(
      reducer({}, {
        type: types.SET_USERS,
        payload: [{id: 1}, {id: 2}]
      })
    ).toEqual({
        users: [{id: 1}, {id: 2}],
    })
    expect(
      reducer({users: [{id: 1, name: 'one'}, {id: 2, name: 'two'}]},
        {
          type: types.SET_USER,
          payload: {id: 1, name: 'changed'}
        }
      )
    ).toEqual({
        users: [{id: 1, name: 'changed'}, {id: 2, name: 'two'}]
    })
  })

  it('should handle SET_BASKET', () => {
    expect(
      reducer({}, {
        type: types.SET_BASKET,
        payload: ['Apple 1', 'Apple 2']
      })
    ).toEqual({
        basket: ['Apple 1', 'Apple 2'],
    })
  })

  it('should handle FETCH_FAILURE', () => {
    expect(
      reducer({}, {
        type: types.FETCH_FAILURE,
        error: 'some error'
      })
    ).toEqual({
        error: 'some error',
    })
  })

})
