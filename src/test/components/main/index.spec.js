import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import {MainComponent} from '../../../components/main/index';
import {Wrapper, FreeButton, Error} from '../../../components/main/styles';

Enzyme.configure({ adapter: new Adapter() });

describe('MainComponent', () => {
    let props = {
        fetchBasket: jest.fn(),
        fetchUsers: jest.fn(),
        resetBasket: jest.fn(),
    };
    describe('on render', () => {
        it('should render Wrapper element', () => {
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find(Wrapper).length).toEqual(1);
        });
        it('should have Users element', () => {
            props = {...props};
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find('Users').length).toEqual(1);
        });
        it('should have Basket element', () => {
            props = {...props};
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find('Basket').length).toEqual(1);
        });
        it('should have Error element', () => {
            props = {...props, error: 'someError'};
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find(Error).props().children).toEqual('someError');
        });
        it('should have FreeButton element', () => {
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find(FreeButton).length).toEqual(1);
        });

        it('should pass users props to Users element', () => {
            const users = [{id: 1}, {id: 2}]
            props = {...props, users};
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find('Users').props().users).toEqual(users);
        });

        it('should pass basket props to Basket element', () => {
            const basket = ['one', 'two', 'three'];
            props = {...props, basket};
            const wrapper = shallow(<MainComponent {...props} />);

            expect(wrapper.find('Basket').props().basket).toEqual(basket);
        });

    });
    describe('ComponentDidMount', () => {
        it('should call fetchBasket and fetchUsers props', () => {
            const wrapper = shallow(<MainComponent  {...props} />);
            props.fetchBasket.mockClear();
            props.fetchUsers.mockClear();

            wrapper.instance().componentDidMount();

            expect(props.fetchBasket.mock.calls.length).toEqual(1);
            expect(props.fetchUsers.mock.calls.length).toEqual(1);
        });
    });
    describe('when FreeButton is clicked', () => {
        it('should call resetBasket prop', () => {
            const wrapper = shallow(<MainComponent  {...props} />);
            props.resetBasket.mockClear();

            wrapper.find(FreeButton).at(0).simulate('click');

            expect(props.resetBasket.mock.calls.length).toEqual(1);
        });
    });
});
