import grabApple from '../../actions/grab-apple';
import Types from '../../constants/actions';

describe('actions', () => {
    it('should create an action to grab an apple by user', () => {
        const userId = 1;
        const expectedAction = {
            type: Types.GRAB_APPLE,
            userId,
        };
        expect(grabApple(userId)).toEqual(expectedAction);
    });
});

