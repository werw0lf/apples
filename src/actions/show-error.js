import Actions from '../constants/actions';

const actionCreator = (payload) => ({
    type: Actions.SHOW_ERROR,
    payload,
});

export default actionCreator;
