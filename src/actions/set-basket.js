import Actions from '../constants/actions';

const actionCreator = (payload) => ({
    type: Actions.SET_BASKET,
    payload,
});

export default actionCreator;
