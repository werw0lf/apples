import Actions from '../constants/actions';

const actionCreator = () => ({
    type: Actions.RESET_BASKET,
});

export default actionCreator;
