import Actions from '../constants/actions';

const actionCreator = (payload) => ({
    type: Actions.SET_USER,
    payload,
});

export default actionCreator;
