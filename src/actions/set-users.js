import Actions from '../constants/actions';

const actionCreator = (payload) => ({
    type: Actions.SET_USERS,
    payload,
});

export default actionCreator;
