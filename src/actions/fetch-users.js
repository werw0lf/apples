import Actions from '../constants/actions';

const actionCreator = () => ({
    type: Actions.FETCH_USERS,
});

export default actionCreator;
