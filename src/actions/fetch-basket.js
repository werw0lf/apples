import Actions from '../constants/actions';

const actionCreator = () => ({
    type: Actions.FETCH_BASKET,
});

export default actionCreator;
