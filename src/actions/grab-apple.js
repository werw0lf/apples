import Actions from '../constants/actions';

const actionCreator = (userId) => ({
    type: Actions.GRAB_APPLE,
    userId,
});

export default actionCreator;
