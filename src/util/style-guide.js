export default function toggleProp(key, on, off) {
    return (props) => props[key] ? on : off;
}
