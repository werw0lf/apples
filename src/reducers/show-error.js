import Actions from '../constants/actions';

export default function (currentState, action) {
    if (action.type !== Actions.SHOW_ERROR) {
        return currentState;
    }

    return action.payload;
}
