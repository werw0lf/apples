import fetchError from './error';
import setUsers from './set-users';
import setBasket from './set-basket';
import showError from './show-error';

export default function (currentState, action) {
    return {
        users: setUsers(currentState ? currentState.users : [], action),
        basket: setBasket(currentState ? currentState.basket : [], action),
        error: fetchError(currentState ? currentState.error : '', action),
        showError: showError(currentState ? currentState.showError : false, action),
    };
}
