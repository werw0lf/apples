import Actions from '../constants/actions';

export default function (currentState, action) {
    if (action.type === Actions.SET_USER) {
        if (!action.payload) {
            return currentState;
        }

        const index = currentState.findIndex(u => u.id === action.payload.id);
        const users = currentState;
        users[index] = action.payload;

        return [...users];
    }

    if (action.type !== Actions.SET_USERS) {
        return currentState;
    }

    return action.payload;
}
