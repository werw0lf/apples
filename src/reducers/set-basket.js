import Actions from '../constants/actions';

export default function (currentState, action) {
    if (action.type !== Actions.SET_BASKET) {
        return currentState;
    }

    return action.payload;
}
