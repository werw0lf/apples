import React, {Component} from 'react';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import fetchBasket from '../../actions/fetch-basket';
import fetchUsers from '../../actions/fetch-users';
import resetBasket from '../../actions/reset-basket';
import grabApple from '../../actions/grab-apple';
import Users from '../../components/users';
import Basket from '../../components/basket';
import {Column} from 'styled-bootstrap-components';

import {
    Wrapper,
    FreeButton,
    ButtonWrap,
    Header,
    ColumnAlerts,
    Error,
} from './styles';

export class MainComponent extends Component {
    componentDidMount() {
        this.props.fetchBasket();
        this.props.fetchUsers();
    }

    render() {
        return (
            <Wrapper>
                <ColumnAlerts md={12} empty={!this.props.showError}>
                    <Error danger>{this.props.error}</Error>
                </ColumnAlerts>

                <Column md={6}>
                    <Header>Users</Header>
                    <Users users={this.props.users} grabApple={this.props.grabApple} />
                    <ButtonWrap><FreeButton onClick={this.props.resetBasket}>Free Apples</FreeButton></ButtonWrap>
                </Column>
                <Column md={6}>
                    <Header>Basket</Header>
                    <Basket basket={this.props.basket} />
                </Column>
            </Wrapper>
        );
    }
}

const mapStateToProps = (state) => ({
    ...state,
});

MainComponent.propTypes = {
    basket: propTypes.instanceOf(Array),
    users: propTypes.instanceOf(Array),
    error: propTypes.string,
    showError: propTypes.bool,
    fetchBasket: propTypes.func,
    fetchUsers: propTypes.func,
    resetBasket: propTypes.func,
};

export default connect(mapStateToProps, {fetchBasket, resetBasket, fetchUsers, grabApple})(MainComponent);
