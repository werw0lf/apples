import styled from 'styled-components';
import { Row, Column } from 'styled-grid-system-component';
import { Alert } from 'styled-bootstrap-components';
import toggleProp from '../../util/style-guide';

export const Wrapper = styled(Row)`
  font-family: "Nunito", sans-serif;
  font-size: 0.9rem;
`

export const FreeButton = styled.button`
    color: #fff;
    background-color: #38c172;
    border-color: #38c172;
    padding: 0.5rem 1rem;
    font-size: 1.125rem;
    line-height: 1.5;
    border-radius: 0.3rem;
    cursor: pointer;
    transition: background 0.3s ease;
    &:focus { outline: none; }
    &:hover {
        background-color: #2fa360;
        border-color: #2d995b;
        }
`;

export const ButtonWrap = styled.div`
    text-align: right;
    padding-top: 15px;
`;

export const Header = styled.h3`
  color: #95a5a6;
  font-weight: 400;
  font-size: 1.575rem;
  line-height: 1.2;
  margin-bottom: 0.5rem;
`;

export const ColumnAlerts = styled(Column)`
  padding-top: 10px;
  min-height: 75px;
  opacity: ${toggleProp('empty', '0', '1')};
  transition: opacity 0.3s ease;
`;

export const Error = styled(Alert)`
  border-radius: 0.25rem;
  color: #721c24;
`;
