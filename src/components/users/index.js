import React from 'react';
import propTypes from 'prop-types';
import {Badge, ListGroup, ListGroupItem} from 'styled-bootstrap-components';
import {UserBlock, GrabButton} from './styles'

const Users = ({users, grabApple}) => (
    <ListGroup>
        {users && users.map((user) => (
            <ListGroupItem key={user.id}>
                <UserBlock>
                    {user.name}
                    <span>
                        <Badge pill primary>{user.apples.length}</Badge>
                        <GrabButton sm danger onClick={() => grabApple(user.id)}>Grab apple</GrabButton>
                    </span>
                </UserBlock>
                <ul>
                    {user.apples && user.apples.map(apple => <li key={apple.id}>Apple {apple.id}</li>)}
                </ul>
            </ListGroupItem>
        ))}
    </ListGroup>
);

Users.propTypes = {
    users: propTypes.instanceOf(Array),
    grabApple: propTypes.func,
};

export default Users;
