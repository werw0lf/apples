import styled from 'styled-components';
import {Button} from 'styled-bootstrap-components';

export const UserBlock = styled.div`
    align-items: center;
    justify-content: space-between;
    display: flex;
`;

export const GrabButton = styled(Button)`
    margin-left: 10px;
`;
