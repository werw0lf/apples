import React from 'react';
import propTypes from 'prop-types';
import {ListGroup, ListGroupItem} from 'styled-bootstrap-components';

const Basket = ({basket}) => (
    <ListGroup>
        {basket && basket.map((apple, id) =>
            <ListGroupItem key={id}>{apple}</ListGroupItem>
        )}
    </ListGroup>
);

Basket.propTypes = {
    Basket: propTypes.instanceOf(Array),
};

export default Basket;
