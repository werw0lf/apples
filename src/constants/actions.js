export default {
    INIT: '@@INIT',
    FETCH_BASKET: 'Fetch Basket',
    FETCH_USERS: 'Fetch Users',
    FETCH_FAILURE: 'Fetch Failure',
    RESET_BASKET: 'Reset Basket',
    GRAB_APPLE: 'Grab apple by user',
    SET_USERS: 'Set users',
    SET_BASKET: 'Set basket',
    SET_USER: 'Set user',
    SHOW_ERROR: 'Show error',
};
