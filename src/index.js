import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './components/main';
import * as serviceWorker from './serviceWorker';
import configureStore from './store';
import {Provider} from 'react-redux';
import { Navbar, NavbarLink } from 'styled-navbar-component';
import { Container } from 'styled-container-component';

ReactDOM.render(
    <Provider store={configureStore()}>
        <Navbar light>
            <Container>
                <NavbarLink brand>Alyce - Test Task</NavbarLink>
            </Container>
        </Navbar>
        <Container>
            <Main />
        </Container>
    </Provider>,

    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
