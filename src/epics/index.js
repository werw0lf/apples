import {combineEpics} from 'redux-observable';
import {fetchBasket, fetchUsers, grabApple, resetBasket} from './apples';

export default combineEpics(
    fetchBasket,
    fetchUsers,
    grabApple,
    resetBasket,
);
