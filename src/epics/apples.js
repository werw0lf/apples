import {of, merge} from 'rxjs';
import {switchMap, map, flatMap, catchError, delay} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
import {ofType} from 'redux-observable';
import Actions from '../constants/actions';
import setBasket from '../actions/set-basket';
import setUsers from '../actions/set-users';
import setUser from '../actions/set-user';
import fetchFailure from '../actions/fetch-failure';
import fetchBasketAction from '../actions/fetch-basket';
import fetchUsersAction from '../actions/fetch-users';
import showError from '../actions/show-error';

const PREFIX = 'https://cors-anywhere.herokuapp.com/';

export function fetchBasket(action$) {
    return action$.pipe(
        ofType(Actions.FETCH_BASKET),
        switchMap(() =>
            ajax.getJSON(`${PREFIX}http://hrtest.alycedev.com/basket`).pipe(
                map((response) => setBasket(response)),
                catchError(errorHandler)
            ))
    );
}

export function fetchUsers(action$) {
    return action$.pipe(
        ofType(Actions.FETCH_USERS),
        switchMap(() =>
            ajax.getJSON(`${PREFIX}http://hrtest.alycedev.com/users`).pipe(
                map((response) => setUsers(response)),
                catchError(errorHandler)
            ))
    );
}

export function resetBasket(action$) {
    return action$.pipe(
        ofType(Actions.RESET_BASKET),
        switchMap(() =>
            ajax.getJSON(`${PREFIX}http://hrtest.alycedev.com/apples/free`).pipe(
                flatMap(() => [fetchUsersAction(), fetchBasketAction()]),
                catchError(errorHandler)
            ))
    );
}

export function grabApple(action$) {
    return action$.pipe(
        ofType(Actions.GRAB_APPLE),
        switchMap((action) =>
            ajax.getJSON(`${PREFIX}http://hrtest.alycedev.com/users/${action.userId}/grab`).pipe(
                flatMap((response) => {
                    if (response.success) {
                        return [fetchBasketAction(), setUser(response.user)];
                     } else
                        throw new Error(response.message);
                }),
                catchError(errorHandler)
            )
        )
    )
}

function errorHandler(error) {
    return merge(
        of(showError(true)),
        of(fetchFailure(error.message)),
        of(showError(false)).pipe(delay(4000)),
    );
}
